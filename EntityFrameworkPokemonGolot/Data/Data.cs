using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;


#nullable disable

namespace Connexio
{
    public partial class pokemongoolotContext : DbContext
    {
        public pokemongoolotContext()
        {
        }

        public pokemongoolotContext(DbContextOptions<pokemongoolotContext> options)
            : base(options) 
        {
        }
        public virtual DbSet<Usuari> Usuaris { get; set; }
        public virtual DbSet<Equip> Equips { get; set; }
        public virtual DbSet<UsuariAmic> UsuariAmics { get; set; }
        public virtual DbSet<Pokemon> Pokemons { get; set; }
        public virtual DbSet<Habilitat> Habilitats { get; set; }
        public virtual DbSet<HabilitatsPokemon> HabilitatsPokemons { get; set; }
        public virtual DbSet<Tipu> Tipus { get; set; }
        public virtual DbSet<Objecte> Objectes { get; set; }
        public virtual DbSet<ObjecteUsuari> ObjecteUsuaris { get; set; }
        public virtual DbSet<UsuariPokemon> UsuariPokemons { get; set; }
        public virtual DbSet<Familia> Familia {get; set;}
         public virtual DbSet<Pokeparada> Pokeparada { get; set; }

        public virtual DbSet<PokedexUsuari> Pokedex {get; set; }

        // public virtual DbSet<PokeparadaGym> PokeparadaGyms { get; set; }
     
 
        public virtual DbSet<UsuariPokemonCaramel> UsuariPokemonCaramels { get; set; }
        public virtual DbSet<PokedexUsuari> PokedexUsuaris { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // optionsBuilder.UseNpgsql("Host=172.24.127.2;Port=5432;Database=PokemonGoOlotEntornProves;Username=postgres;Password=Patata123");
                optionsBuilder.UseNpgsql("Host=127.0.0.1;Port=5432;Database=PokemonGoOlotEntornProves;Username=postgres;Password=Patata44");

                // optionsBuilder.UseNpgsql(Decrypt.DecryptFile());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Primary Keys
            modelBuilder.Entity<Usuari>().HasKey(x => x.NomUsuari);
            modelBuilder.Entity<Equip>().HasKey(x => x.NomEquip);
            modelBuilder.Entity<UsuariAmic>().HasKey(x => new {x.UsuariAmic1,x.UsuariAmic2});
            modelBuilder.Entity<Pokemon>().HasKey(x => x.NumPokedex);
            modelBuilder.Entity<HabilitatsPokemon>().HasKey(x => new {x.NumPokedex,x.NomHabilitat});
            modelBuilder.Entity<UsuariPokemon>().HasKey(x => x.IdUsuariPokemon);
            modelBuilder.Entity<Habilitat>().HasKey(x => x.NomHabilitat);
            modelBuilder.Entity<Tipu>().HasKey(x=> x.NomTipus);
            modelBuilder.Entity<Objecte>().HasKey(x=> x.NomObjecte);
            modelBuilder.Entity<ObjecteUsuari>().HasKey(x => new {x.NomObjecte,x.NomUsuari});
            modelBuilder.Entity<UsuariPokemonCaramel>().HasKey(x=> new {x.NomUsuari,x.NumPokemon});
            modelBuilder.Entity<Familia>().HasKey(x => new {x.NomFamilia});
            modelBuilder.Entity<Pokeparada>().HasKey(x => new {x.Longitud,x.Latitud});
            modelBuilder.Entity<PokedexUsuari>().HasKey(x => x.NumPokedex);
        
            // Foreings KEYS
            modelBuilder.Entity<Usuari>().HasOne(x => x.Equip).WithMany(c => c.Usuaris).HasForeignKey(x => x.NomEquip);
            modelBuilder.Entity<UsuariAmic>().HasOne(x=> x.Usuari1).WithMany(c => c.Amic1).HasForeignKey(x => x.UsuariAmic1);
            modelBuilder.Entity<UsuariAmic>().HasOne(x=> x.Usuari2).WithMany(c => c.Amic2).HasForeignKey(x => x.UsuariAmic2);
            modelBuilder.Entity<Habilitat>().HasOne(x => x.Tipus).WithMany(c => c.Habilitat).HasForeignKey(x => x.TipusHabilitat);
            modelBuilder.Entity<Pokemon>().HasOne(x => x.Tipus).WithMany(c => c.Pokemons).HasForeignKey(x => x.TipusPokemon);
           
            modelBuilder.Entity<UsuariPokemonCaramel>().HasOne(x => x.NumPokemonNavigation).WithMany(c => c.Caramel).HasForeignKey(x => x.NumPokemon);
            modelBuilder.Entity<UsuariPokemonCaramel>().HasOne(x => x.NomUsuariNavigation).WithMany(c=> c.UsuariPokemonCaramels).HasForeignKey(x => x.NomUsuari);
            //ObjecteUsuari
            modelBuilder.Entity<ObjecteUsuari>().HasOne(x => x.Objecte).WithMany(c => c.ObjecteUsuaris).HasForeignKey(x => x.NomObjecte);
            modelBuilder.Entity<ObjecteUsuari>().HasOne(x => x.Usuari).WithMany(c => c.ObjecteUsuaris).HasForeignKey(x => x.NomUsuari);
            //UsuariPokemon
            modelBuilder.Entity<UsuariPokemon>().HasOne(x => x.HabilitatPokemonCarrega).WithMany(x => x.UsuariPokemonHabilitatCarregat).HasForeignKey(x => x.HabilitatPokemonCarregat);
            modelBuilder.Entity<UsuariPokemon>().HasOne(x => x.HabilitatPokemonSimp).WithMany(x => x.UsuariPokemonHabilitatSimple).HasForeignKey(x => x.HabilitatPokemonSimple);
            modelBuilder.Entity<UsuariPokemon>().HasOne(x => x.Usuari).WithMany(x => x.UsuariPokemons).HasForeignKey(x => x.NomUsuari);
            modelBuilder.Entity<UsuariPokemon>().HasOne(x => x.Pokemon).WithMany(x => x.UsuariPokemons).HasForeignKey(x => x.NumPokedex);
            
            modelBuilder.Entity<PokedexUsuari>().HasOne(x => x.Pokemon).WithMany(x => x.PokedexUsuaris).HasForeignKey(x => x.NumPokedex);
            modelBuilder.Entity<PokedexUsuari>().HasOne(x => x.Usuari).WithMany(x => x.PokedexUsuaris).HasForeignKey(x => x.NomUsuari);
           
            // ADD data
            // modelBuilder.Entity<Tipu>().HasData(AddType.AddTypes());
            // modelBuilder.Entity<Pokemon>().HasData(AddPokemon.AddPokemons());
            // modelBuilder.Entity<Habilitat>().HasData(AddMoves.Moves());

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
