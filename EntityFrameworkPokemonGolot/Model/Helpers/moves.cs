using System.Collections.Generic;

namespace Connexio
{
    public class Moves
    {
        public int duration { get; set; }
        public int energy_delta { get; set; }
        public int move_id { get; set; }
        public string name { get; set; }
        public int power { get; set; }
        public double stamina_loss_scaler { get; set; }
        public string type { get; set; }
    }


}