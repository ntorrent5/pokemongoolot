﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Connexio.Migrations
{
    public partial class Prova : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Equips",
                columns: table => new
                {
                    nom_equip = table.Column<string>(type: "varchar(30)", nullable: false),
                    img_equip = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equips", x => x.nom_equip);
                });

            migrationBuilder.CreateTable(
                name: "Familia",
                columns: table => new
                {
                    NomFamilia = table.Column<string>(type: "varchar(40)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Familia", x => x.NomFamilia);
                });

            migrationBuilder.CreateTable(
                name: "Objectes",
                columns: table => new
                {
                    nom_objecte = table.Column<string>(type: "varchar(30)", nullable: false),
                    descripcio_objecte = table.Column<string>(type: "varchar(300)", nullable: true),
                    img_objecte = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Objectes", x => x.nom_objecte);
                });

            migrationBuilder.CreateTable(
                name: "Pokeparada",
                columns: table => new
                {
                    latitud = table.Column<double>(type: "double precision", nullable: false),
                    longitud = table.Column<double>(type: "double precision", nullable: false),
                    nom_pokeparada = table.Column<string>(type: "varchar(40)", nullable: true),
                    descripcio_pokeparada = table.Column<string>(type: "varchar(300)", nullable: true),
                    img_pokeparada = table.Column<string>(type: "varchar(100)", nullable: true),
                    tipus_pokeparada = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pokeparada", x => new { x.longitud, x.latitud });
                });

            migrationBuilder.CreateTable(
                name: "Tipus",
                columns: table => new
                {
                    nom_tipus = table.Column<string>(type: "varchar(40)", nullable: false),
                    nom_color = table.Column<string>(type: "varchar(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tipus", x => x.nom_tipus);
                });

            migrationBuilder.CreateTable(
                name: "Usuaris",
                columns: table => new
                {
                    nom_usuari = table.Column<string>(type: "varchar(25)", nullable: false),
                    password = table.Column<string>(type: "varchar(200)", nullable: true),
                    nivell = table.Column<double>(type: "double precision", nullable: true),
                    data_creacio = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    distancia = table.Column<double>(type: "double precision", nullable: true),
                    polvos_estelares = table.Column<int>(type: "integer", nullable: true),
                    is_online = table.Column<bool>(type: "boolean", nullable: true),
                    nom_equip = table.Column<string>(type: "varchar(30)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuaris", x => x.nom_usuari);
                    table.ForeignKey(
                        name: "FK_Usuaris_Equips_nom_equip",
                        column: x => x.nom_equip,
                        principalTable: "Equips",
                        principalColumn: "nom_equip",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Habilitats",
                columns: table => new
                {
                    nom_habilitat = table.Column<string>(type: "varchar(40)", nullable: false),
                    tipus_habilitat = table.Column<string>(type: "varchar(40)", nullable: true),
                    dmg = table.Column<int>(type: "integer", nullable: true),
                    descripcio_habilitat = table.Column<string>(type: "varchar(150)", nullable: true),
                    isCarregat = table.Column<bool>(type: "boolean", nullable: false),
                    temps_habilitat = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Habilitats", x => x.nom_habilitat);
                    table.ForeignKey(
                        name: "FK_Habilitats_Tipus_tipus_habilitat",
                        column: x => x.tipus_habilitat,
                        principalTable: "Tipus",
                        principalColumn: "nom_tipus",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Pokemons",
                columns: table => new
                {
                    num_pokedex = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    nom_pokemon = table.Column<string>(type: "varchar(40)", nullable: true),
                    descripcio_pokemon = table.Column<string>(type: "varchar(300)", nullable: true),
                    img_pokemon = table.Column<string>(type: "varchar(200)", nullable: true),
                    tipus_pokemon = table.Column<string>(type: "varchar(40)", nullable: true),
                    ps_min = table.Column<int>(type: "integer", nullable: true),
                    pc_min = table.Column<int>(type: "integer", nullable: true),
                    defensa_base = table.Column<int>(type: "integer", nullable: true),
                    atac_base = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pokemons", x => x.num_pokedex);
                    table.ForeignKey(
                        name: "FK_Pokemons_Tipus_tipus_pokemon",
                        column: x => x.tipus_pokemon,
                        principalTable: "Tipus",
                        principalColumn: "nom_tipus",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ObjecteUsuaris",
                columns: table => new
                {
                    nom_usuari = table.Column<string>(type: "varchar(25)", nullable: false),
                    nom_objecte = table.Column<string>(type: "varchar(25)", nullable: false),
                    quantitat = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjecteUsuaris", x => new { x.nom_objecte, x.nom_usuari });
                    table.ForeignKey(
                        name: "FK_ObjecteUsuaris_Objectes_nom_objecte",
                        column: x => x.nom_objecte,
                        principalTable: "Objectes",
                        principalColumn: "nom_objecte",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ObjecteUsuaris_Usuaris_nom_usuari",
                        column: x => x.nom_usuari,
                        principalTable: "Usuaris",
                        principalColumn: "nom_usuari",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UsuariAmics",
                columns: table => new
                {
                    nom_usuari_amic_1 = table.Column<string>(type: "varchar(25)", nullable: false),
                    nom_usuari_amic_2 = table.Column<string>(type: "varchar(25)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuariAmics", x => new { x.nom_usuari_amic_1, x.nom_usuari_amic_2 });
                    table.ForeignKey(
                        name: "FK_UsuariAmics_Usuaris_nom_usuari_amic_1",
                        column: x => x.nom_usuari_amic_1,
                        principalTable: "Usuaris",
                        principalColumn: "nom_usuari",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsuariAmics_Usuaris_nom_usuari_amic_2",
                        column: x => x.nom_usuari_amic_2,
                        principalTable: "Usuaris",
                        principalColumn: "nom_usuari",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FamiliaPokemon",
                columns: table => new
                {
                    FamiliaNomFamilia = table.Column<string>(type: "varchar(40)", nullable: false),
                    PokemonsNumPokedex = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FamiliaPokemon", x => new { x.FamiliaNomFamilia, x.PokemonsNumPokedex });
                    table.ForeignKey(
                        name: "FK_FamiliaPokemon_Familia_FamiliaNomFamilia",
                        column: x => x.FamiliaNomFamilia,
                        principalTable: "Familia",
                        principalColumn: "NomFamilia",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FamiliaPokemon_Pokemons_PokemonsNumPokedex",
                        column: x => x.PokemonsNumPokedex,
                        principalTable: "Pokemons",
                        principalColumn: "num_pokedex",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HabilitatsPokemons",
                columns: table => new
                {
                    num_pokedex = table.Column<int>(type: "integer", nullable: false),
                    nom_habilitat = table.Column<string>(type: "varchar(40)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HabilitatsPokemons", x => new { x.num_pokedex, x.nom_habilitat });
                    table.ForeignKey(
                        name: "FK_HabilitatsPokemons_Habilitats_nom_habilitat",
                        column: x => x.nom_habilitat,
                        principalTable: "Habilitats",
                        principalColumn: "nom_habilitat",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HabilitatsPokemons_Pokemons_num_pokedex",
                        column: x => x.num_pokedex,
                        principalTable: "Pokemons",
                        principalColumn: "num_pokedex",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PokedexUsuari",
                columns: table => new
                {
                    num_pokedex = table.Column<int>(type: "integer", nullable: false),
                    nom_usuari = table.Column<string>(type: "varchar(25)", nullable: true),
                    data_visualitzacio = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    cops_atrapats = table.Column<int>(type: "integer", nullable: true),
                    hasSeenShiny = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PokedexUsuari", x => x.num_pokedex);
                    table.ForeignKey(
                        name: "FK_PokedexUsuari_Pokemons_num_pokedex",
                        column: x => x.num_pokedex,
                        principalTable: "Pokemons",
                        principalColumn: "num_pokedex",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PokedexUsuari_Usuaris_nom_usuari",
                        column: x => x.nom_usuari,
                        principalTable: "Usuaris",
                        principalColumn: "nom_usuari",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UsuariPokemons",
                columns: table => new
                {
                    id_usuari_pokemon = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    nom_usuari = table.Column<string>(type: "varchar(25)", nullable: true),
                    data_captura = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    num_pokedex = table.Column<int>(type: "integer", nullable: false),
                    pc = table.Column<int>(type: "integer", nullable: true),
                    ps = table.Column<int>(type: "integer", nullable: true),
                    habilitat_pokemon_simple = table.Column<string>(type: "varchar(40)", nullable: true),
                    habilitat_pokemon_carregat = table.Column<string>(type: "varchar(40)", nullable: true),
                    is_in_gym = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuariPokemons", x => x.id_usuari_pokemon);
                    table.ForeignKey(
                        name: "FK_UsuariPokemons_Habilitats_habilitat_pokemon_carregat",
                        column: x => x.habilitat_pokemon_carregat,
                        principalTable: "Habilitats",
                        principalColumn: "nom_habilitat",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UsuariPokemons_Habilitats_habilitat_pokemon_simple",
                        column: x => x.habilitat_pokemon_simple,
                        principalTable: "Habilitats",
                        principalColumn: "nom_habilitat",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UsuariPokemons_Pokemons_num_pokedex",
                        column: x => x.num_pokedex,
                        principalTable: "Pokemons",
                        principalColumn: "num_pokedex",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsuariPokemons_Usuaris_nom_usuari",
                        column: x => x.nom_usuari,
                        principalTable: "Usuaris",
                        principalColumn: "nom_usuari",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UsuariPokemonCaramels",
                columns: table => new
                {
                    nom_usuari = table.Column<string>(type: "varchar(25)", nullable: false),
                    num_pokemon = table.Column<int>(type: "integer", nullable: false),
                    quantitat = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuariPokemonCaramels", x => new { x.nom_usuari, x.num_pokemon });
                    table.ForeignKey(
                        name: "FK_UsuariPokemonCaramels_UsuariPokemons_num_pokemon",
                        column: x => x.num_pokemon,
                        principalTable: "UsuariPokemons",
                        principalColumn: "id_usuari_pokemon",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsuariPokemonCaramels_Usuaris_nom_usuari",
                        column: x => x.nom_usuari,
                        principalTable: "Usuaris",
                        principalColumn: "nom_usuari",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FamiliaPokemon_PokemonsNumPokedex",
                table: "FamiliaPokemon",
                column: "PokemonsNumPokedex");

            migrationBuilder.CreateIndex(
                name: "IX_Habilitats_tipus_habilitat",
                table: "Habilitats",
                column: "tipus_habilitat");

            migrationBuilder.CreateIndex(
                name: "IX_HabilitatsPokemons_nom_habilitat",
                table: "HabilitatsPokemons",
                column: "nom_habilitat");

            migrationBuilder.CreateIndex(
                name: "IX_ObjecteUsuaris_nom_usuari",
                table: "ObjecteUsuaris",
                column: "nom_usuari");

            migrationBuilder.CreateIndex(
                name: "IX_PokedexUsuari_nom_usuari",
                table: "PokedexUsuari",
                column: "nom_usuari");

            migrationBuilder.CreateIndex(
                name: "IX_Pokemons_tipus_pokemon",
                table: "Pokemons",
                column: "tipus_pokemon");

            migrationBuilder.CreateIndex(
                name: "IX_UsuariAmics_nom_usuari_amic_2",
                table: "UsuariAmics",
                column: "nom_usuari_amic_2");

            migrationBuilder.CreateIndex(
                name: "IX_UsuariPokemonCaramels_num_pokemon",
                table: "UsuariPokemonCaramels",
                column: "num_pokemon");

            migrationBuilder.CreateIndex(
                name: "IX_UsuariPokemons_habilitat_pokemon_carregat",
                table: "UsuariPokemons",
                column: "habilitat_pokemon_carregat");

            migrationBuilder.CreateIndex(
                name: "IX_UsuariPokemons_habilitat_pokemon_simple",
                table: "UsuariPokemons",
                column: "habilitat_pokemon_simple");

            migrationBuilder.CreateIndex(
                name: "IX_UsuariPokemons_nom_usuari",
                table: "UsuariPokemons",
                column: "nom_usuari");

            migrationBuilder.CreateIndex(
                name: "IX_UsuariPokemons_num_pokedex",
                table: "UsuariPokemons",
                column: "num_pokedex");

            migrationBuilder.CreateIndex(
                name: "IX_Usuaris_nom_equip",
                table: "Usuaris",
                column: "nom_equip");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FamiliaPokemon");

            migrationBuilder.DropTable(
                name: "HabilitatsPokemons");

            migrationBuilder.DropTable(
                name: "ObjecteUsuaris");

            migrationBuilder.DropTable(
                name: "PokedexUsuari");

            migrationBuilder.DropTable(
                name: "Pokeparada");

            migrationBuilder.DropTable(
                name: "UsuariAmics");

            migrationBuilder.DropTable(
                name: "UsuariPokemonCaramels");

            migrationBuilder.DropTable(
                name: "Familia");

            migrationBuilder.DropTable(
                name: "Objectes");

            migrationBuilder.DropTable(
                name: "UsuariPokemons");

            migrationBuilder.DropTable(
                name: "Habilitats");

            migrationBuilder.DropTable(
                name: "Pokemons");

            migrationBuilder.DropTable(
                name: "Usuaris");

            migrationBuilder.DropTable(
                name: "Tipus");

            migrationBuilder.DropTable(
                name: "Equips");
        }
    }
}
