﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

    namespace Connexio
{ 
    class Program
    {

        static  void Main(string[] args)
        {
             pokemongoolotContext dbContext = new pokemongoolotContext();

            
            List<Pokemon> pokemons = AddPokemon.AddPokemons();
                foreach( Pokemon data in pokemons){
                    dbContext.Pokemons.Add(data);                
                }
            List<Habilitat> moves = AddMoves.Moves();
                foreach( Habilitat data in moves){
                    dbContext.Habilitats.Add(data);
                }
            List<Tipu> types =  AddType.AddTypes();
                foreach(Tipu data in types){
                    dbContext.Tipus.Add(data);
                }
            List<Equip> equips = AddTeam.AddTypes();
                foreach(Equip  data in equips){
                    dbContext.Equips.Add(data);
                }
            dbContext.SaveChanges();
        } 
    }
}


