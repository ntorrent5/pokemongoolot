﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
#nullable disable

namespace Connexio
{
    public partial class UsuariAmic
    {
        [Key]
        [Column("nom_usuari_amic_1",TypeName ="varchar(25)")]
        public string UsuariAmic1 { get; set; }
        [Key]
        [Column("nom_usuari_amic_2",TypeName ="varchar(25)")]
        public string UsuariAmic2 { get; set; }

        public virtual Usuari Usuari1 { get; set; }
        public virtual Usuari Usuari2 { get; set; }
    }
}
