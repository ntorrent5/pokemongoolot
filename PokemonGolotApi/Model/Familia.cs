﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace Connexio
{
    public partial class Familia
    {   
        [Column("NomFamilia",TypeName ="varchar(40)")]
        public string NomFamilia {get; set;}

      
        public virtual ICollection<Pokemon> Pokemons {get; set;}

    }
}
