﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace Connexio
{
    public partial class Usuari
    {
       
        [Key]
        [Column("nom_usuari",TypeName ="varchar(25)")]
        public string NomUsuari { get; set; }
        [Column("password",TypeName = "varchar(200)")]
        public string Password { get; set;}
        [Column("nivell")]
        public double? Nivell { get; set; }
        [Column("data_creacio")]
        public DateTime? DataCreacio { get; set; }
        [Column("distancia")]
        public double? Distancia { get; set; }
        [Column("polvos_estelares")]
        public int? PolvosEstelares { get; set; }
        [Column("is_online")]
        public bool? IsOnline { get; set; }
        [Column("nom_equip",TypeName ="varchar(30)")]
        public string NomEquip { get; set; }

        public virtual Equip Equip { get; set; }
        public virtual ICollection<UsuariAmic> Amic1 { get; set; }
        public virtual ICollection<UsuariAmic> Amic2 { get; set; }
        public virtual ICollection<UsuariPokemon> UsuariPokemons { get; set; }

        public virtual ICollection<ObjecteUsuari> ObjecteUsuaris { get; set; }
         public virtual ICollection<UsuariPokemonCaramel> UsuariPokemonCaramels { get; set; }
         public virtual ICollection<PokedexUsuari> PokedexUsuaris { get; set; }
        // public virtual ICollection<RegalContingut> RegalContingutEmisorNavigations { get; set; }
        // public virtual ICollection<RegalContingut> RegalContingutReceptorNavigations { get; set; }
    }
}
