using System.Collections.Generic;

namespace Connexio
{
    public class pokemonTypeHelper
   {
        public string form { get; set; }
        public int pokemon_id { get; set; }
        public string pokemon_name { get; set; }
        public List<string> type { get; set; }
    }
}