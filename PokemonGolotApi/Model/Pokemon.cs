﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace Connexio
{
    public partial class Pokemon
    {
        [Column("num_pokedex")]
        public int NumPokedex { get; set; }
        [Column("nom_pokemon",TypeName ="varchar(40)")]
        public string NomPokemon { get; set; }
        [Column("descripcio_pokemon",TypeName ="varchar(300)")]
        public string DescripcioPokemon { get; set; }
        [Column("img_pokemon",TypeName ="varchar(200)")]
        public string ImgPokemon { get; set; }
        [Column("tipus_pokemon",TypeName ="varchar(40)")]
        public string TipusPokemon { get; set; }
        [Column("ps_min")]
        public int? PsMax { get; set; }
        [Column("pc_min")]
        public int? PcMax { get; set; }
        [Column("defensa_base")]
        public int? BaseDefense {get ; set;}
        [Column("atac_base")]
        public int? BaseAttack {get ; set;}


        public virtual Tipu Tipus { get; set; }
        public virtual ICollection<HabilitatsPokemon> HabilitatsPokemons { get; set; }
        public virtual ICollection<UsuariPokemon> UsuariPokemons { get; set; }

        public virtual ICollection<Familia> Familia {get; set;}
        public virtual ICollection<PokedexUsuari> PokedexUsuaris { get; set; }

    }
}
