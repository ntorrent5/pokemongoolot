﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Connexio
{
 class AddMoves 
 {
     public static List<Habilitat> Moves(){
        List<Moves> Moves = new List<Moves>();
        Task<List<Moves>> SoftMoves = GetSoftMoves();
        Task<List<Moves>> StrongMoves = GetStrongMoves();
        SoftMoves.Wait();
        StrongMoves.Wait();
        
        List<Habilitat> habilitats = GetHabilitats(SoftMoves.Result,StrongMoves.Result);
        //Falta formatar dades
        return habilitats;
     }

     public static List<Habilitat> GetHabilitats(List<Moves> SoftMoves , List<Moves> StrongMoves){
        List<Habilitat> habilitats = new List<Habilitat>();
        var Habilitat = new Habilitat();
        foreach( var moviment in SoftMoves){
         Console.WriteLine(moviment.name);
         moviment.type = char.ToLower(moviment.type[0]) + moviment.type.Substring(1); //formatar les dades per les 2 apis
         habilitats.Add(new Habilitat() {NomHabilitat = moviment.name , Dmg = moviment.power,isCarregat = false,TipusHabilitat= moviment.type,TempsHabilitat= moviment.duration});
        }
         foreach( var moviment in StrongMoves){
         Console.WriteLine(moviment.name);
         moviment.type = char.ToLower(moviment.type[0]) + moviment.type.Substring(1); //formatar les dades per les 2 apis
         habilitats.Add(new Habilitat() {NomHabilitat = moviment.name , Dmg = moviment.power,isCarregat = true,TipusHabilitat= moviment.type,TempsHabilitat= moviment.duration});
        }
         return habilitats;
     }


        public static async Task<List<Moves>> GetSoftMoves(){
           HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://pogoapi.net/api/v1");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage apiCall = await client.GetAsync("https://pogoapi.net/api/v1/fast_moves.json");
            if(apiCall.IsSuccessStatusCode){
                var json = await apiCall.Content.ReadAsStringAsync();
                List<Moves> SoftMoves = JsonConvert.DeserializeObject<List<Moves>>(json);
                return SoftMoves;
            }else{
                return null;
            }
        }

        public static async Task<List<Moves>> GetStrongMoves(){
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://pogoapi.net/api/v1/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage apiCall = await client.GetAsync("https://pogoapi.net/api/v1/charged_moves.json");
            if(apiCall.IsSuccessStatusCode){
                var json = await apiCall.Content.ReadAsStringAsync();
                List<Moves> StrongMoves = JsonConvert.DeserializeObject<List<Moves>>(json);
                return StrongMoves;
            }else{ // generar codi d'error
                return null;
            }
            
        }
    }
}