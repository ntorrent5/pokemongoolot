﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Connexio
{
 class AddPokemon 
 {
     public static List<Pokemon> AddPokemons(){
        Task<List<pokemonHelper>> pokemonStats = PokemonStats();
        pokemonStats.Wait();
        Console.WriteLine(pokemonStats.Result);
        Task<List<pokemonTypeHelper>> pokemonType = PokemonType();
        pokemonType.Wait();
        Task<List<pokemonMaxCPHelper>> MaxPc = GetPcMaxPokemon();
        MaxPc.Wait();

        return AddPokemonData(pokemonStats.Result,pokemonType.Result,MaxPc.Result);
     }

    public static List<Pokemon> AddPokemonData(List<pokemonHelper> pokemonStats,List<pokemonTypeHelper> pokemonType,List<pokemonMaxCPHelper> MaxPc){
        int id = 1;
        var Pokemon = new Pokemon();
        List<Pokemon> Pokemons = new List<Pokemon>();
        foreach (var item in pokemonStats)
            {
               if(item.pokemon_id == id){
                 Task<PokeapiPokemonHelper> pokeApi = GetPokemonPokeApi(id);
                 pokeApi.Wait();
                 PokeapiPokemonHelper ImatgePokemon = pokeApi.Result;
                 Console.WriteLine(id);
                Pokemons.Add(new Pokemon() {NumPokedex = item.pokemon_id, NomPokemon = item.pokemon_name, PcMax = item.base_attack,TipusPokemon = ImatgePokemon.types[0].type.name,
                    PsMax = item.base_stamina,BaseAttack = item.base_attack,BaseDefense = item.base_defense,ImgPokemon= ImatgePokemon.sprites.other.OfficialArtwork.front_default.ToString()});
                  id ++; 
                }
                 
            }
        int idPokemon = 1;
        foreach (var pc in MaxPc){
    
            if(pc.pokemon_id == idPokemon){ // Validacio multiples registres per una entrada
                Pokemons[idPokemon].PcMax = pc.max_cp;
                idPokemon ++;
            }
            if(Pokemons.Count <= idPokemon){
                break;
            }
        }         

        return Pokemons;
        }

        public static async Task<List<pokemonMaxCPHelper>> GetPcMaxPokemon(){
           HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://pogoapi.net/api/v1");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage apiCall = await client.GetAsync("https://pogoapi.net/api/v1/pokemon_max_cp.json");
            if(apiCall.IsSuccessStatusCode){
                var json = await apiCall.Content.ReadAsStringAsync();
                List<pokemonMaxCPHelper> PCmax = JsonConvert.DeserializeObject<List<pokemonMaxCPHelper>>(json);
                return PCmax;
            }else{
                return null;
            }
        }

        public static async Task<PokeapiPokemonHelper> GetPokemonPokeApi(int id){
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://pokeapi.co/api/v2/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage apiCall = await client.GetAsync("https://pokeapi.co/api/v2/pokemon/"+id);
            if(apiCall.IsSuccessStatusCode){
                var json = await apiCall.Content.ReadAsStringAsync();
                PokeapiPokemonHelper pokemon = JsonConvert.DeserializeObject<PokeapiPokemonHelper>(json);
                return pokemon;
            }else{ // generar codi d'error
                return null;
            }
            
        }
        public static async  Task<List<pokemonHelper>>  PokemonStats(){
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://pogoapi.net/api/v1");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                Console.WriteLine("e");

            HttpResponseMessage pokemon = await client.GetAsync("https://pogoapi.net/api/v1/pokemon_stats.json");
            if(pokemon.IsSuccessStatusCode){
                var json = await pokemon.Content.ReadAsStringAsync();
                Console.WriteLine(JsonConvert.DeserializeObject<List<pokemonHelper>>(json));
            List<pokemonHelper> pokemons = JsonConvert.DeserializeObject<List<pokemonHelper>>(json);

                return pokemons;
            }else{
                Console.WriteLine("ERROR");
               // return "DEW";
                return null;
            }

        }
       public static async  Task<List<pokemonTypeHelper>>  PokemonType(){
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://pogoapi.net/api/v1");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage pokemon = await client.GetAsync("https://pogoapi.net/api/v1/pokemon_types.json");
            if(pokemon.IsSuccessStatusCode){
                var json = await pokemon.Content.ReadAsStringAsync();
                Console.WriteLine(JsonConvert.DeserializeObject<List<pokemonTypeHelper>>(json));
            List<pokemonTypeHelper> pokemons = JsonConvert.DeserializeObject<List<pokemonTypeHelper>>(json);

                return pokemons;
            }else{
                Console.WriteLine("ERROR");
               // return "DEW";
                return null;
            }

        }
 }
}