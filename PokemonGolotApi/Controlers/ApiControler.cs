using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Connexio;
using System.IO;
using System.Security.Cryptography;
using System.Text;


using Microsoft.Net.Http.Headers;



namespace PokemonGolotApi
{
    [ApiController]
    [Route("")]
    public class ApiController : Controller {
            public pokemongoolotContext dbContext = new pokemongoolotContext();

        [HttpGet]
        [Route("pokemon/getAll")]
        public async Task<List<Pokemon>> GetAll() {
            var myTask = Task.Run(() => dbContext.Pokemons.ToListAsync());
            List<Pokemon> maquines = await myTask;
            return maquines;
        }
        [HttpGet]
        [Route("pokemon/get/{idPokemon}")]
        public async Task<Pokemon> GetAll(int idPokemon) { 
            var myTask = Task.Run(()=> dbContext.Pokemons.Where(m => m.NumPokedex == idPokemon).ToList());
            List<Pokemon> pokemon = await myTask;
            Console.WriteLine("PATATA");
            if(pokemon.Count < 0){ // generar ErrorCode
                return null;
            }
            return pokemon.First();
        }
        /******************* USUARI ***********************/
        [HttpGet]
        [Route("usuari/get/{nomUsuari}")]
        public async Task<Usuari> GetUsuari(string nomUsuari){
            var myTask = Task.Run(() => dbContext.Usuaris.Where(m => m.NomUsuari == nomUsuari).ToList());
            List<Usuari> usuari = await myTask;
            if(usuari.Count < 0){
                return null;
            }else{
                return usuari.First();
            }
        }
        
        
        [HttpPost]
        [Route("usuari/add")]
        public void AddUser([FromBody] Usuari user){
           if(user.Password != null && user.NomUsuari != null){
                if(dbContext.Usuaris.Where(m => m.NomUsuari == user.NomUsuari).ToList().Count == 0){ // si hi ha un usuari amb el mateix nom
                    dbContext.Usuaris.Add(new Usuari {NomUsuari = user.NomUsuari,Password =getHash(user.Password),Distancia = user.Distancia,NomEquip=user.NomEquip,Nivell =1.00,DataCreacio=user.DataCreacio,PolvosEstelares = user.PolvosEstelares});        
                    dbContext.SaveChanges();
                }else{
                    Console.WriteLine("USUARI REPETIT");
                }
            }else{
                Console.WriteLine("USUARI SENSE PASSWORD");
            }
        }

        [HttpGet]
        [Route("usuari/login")]
        public async Task<bool> LoginUser([FromBody] Usuari user){
            if(user.Password != null && user.NomUsuari != null ){
                var myTask = Task.Run(() => dbContext.Usuaris.Where(m => m.NomUsuari == user.NomUsuari && m.Password == getHash(user.Password)).ToList());
                List<Usuari> usuari = await myTask;
                if(usuari.Count > 0){
                    Console.WriteLine("ENTRA TRUE");
                    return true;
                }else{
                    Console.WriteLine("ENTRA FALSE");
                    return false;
                }
            }else{
                Console.WriteLine("ENTRA UN USUARI/PASSOWRD PATATUDO");
                return false;
            }
        }

        public string getHash( string text){
            byte[] hashValue;
            UnicodeEncoding ue = new UnicodeEncoding();
            byte[] messageBytes = ue.GetBytes(text);
            SHA256 shHash = SHA256.Create();
            hashValue = shHash.ComputeHash(messageBytes);
            Console.WriteLine(System.Text.Encoding.UTF8.GetString(hashValue));
            return System.Text.Encoding.UTF8.GetString(hashValue);
        }

        /******************* UsuariPokemon ***********************/

        // [HttpPost]
        // [Route("captura/add/{usuari}/{pokemon}")]
        // public  void AddCaptura(int pokemon,string usuari){
        //  UsuariPokemon usuariPokemon = new UsuariPokemon();
         

        // }


        
       
        // [HttpGet]
        // [Route("Maquina/{velocitat}/{nom}")]
        // public async Task<List<Maquina>> GetAll(int velocitat,string nom) {
        //     var myTask = Task.Run(() => context.Maquina.Where(m => m.velocitat>velocitat && m.Nom.CompareTo(nom)>0).ToList());
        //     List<Maquina> maquines = await myTask;
        //     return maquines;
        // }
    }
}