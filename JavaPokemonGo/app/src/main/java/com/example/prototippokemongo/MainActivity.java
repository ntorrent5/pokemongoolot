package com.example.prototippokemongo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.prototippokemongo.extra.MESSAGE";
    private EditText nom_usuari;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nom_usuari = findViewById(R.id.text_view_username);


    }

    public void launchPrincipal(View view) {
        Intent intent = new Intent(this, Pokemon_Maps_Activity.class);
        String message = nom_usuari.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}