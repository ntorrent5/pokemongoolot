package com.example.prototippokemongo;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;

public class Menu extends AppCompatActivity {
    private ImageView close;
    private Switch change;
    private ConstraintLayout constraintLayout;
    private ImageView inventory;
    private  ImageView pokedex;
    private ImageView pokemonUser;




    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        close = (ImageView) findViewById(R.id.close_button);
        change = (Switch) findViewById(R.id.darkmode_button);
        inventory = (ImageView) findViewById(R.id.bag_button);
        pokedex = (ImageView) findViewById(R.id.pokedex_button);
        pokemonUser = (ImageView) findViewById(R.id.pokemon_button);

        pokemonUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPokemonUser();
            }
        });

        pokedex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPokedex();
            }
        });

        inventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInventory();
            }
        });
        constraintLayout = (ConstraintLayout) findViewById(R.id.background);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeMenu();
            }
        });

    }

    private void openPokemonUser() {
        Intent intent = new Intent(this, PokemonUser.class);
        startActivity(intent);
    }

    public void openPokedex() {

        Intent intent = new Intent(this, Pokedex.class);
        startActivity(intent);
    }


    public void openInventory() {
        Intent intent = new Intent(this, Inventory.class);
        startActivity(intent);
    }

    public void closeMenu() {
        Intent intent = new Intent(this, Pokemon_Maps_Activity.class);
        startActivity(intent);
    }

    public void darkMode(View view) {
        if (view.getId()==R.id.darkmode_button){
            if (change.isChecked()){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }
    }
}

