package com.example.prototippokemongo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class PokedexAdapter extends RecyclerView.Adapter<PokedexAdapter.PokedexViewHolder> {

    String data1[], data2[], data3[];
    int images[];
    Context context;

    public PokedexAdapter(Context ct, String s1[], String s2[], String s3[], int img[]){

        context = ct;
        data1 = s1;
        data2 = s2;
        data3 = s3;
        images = img;

    }


    @Override
    public PokedexViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_pokedex, parent, false);
        return new PokedexAdapter.PokedexViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PokedexViewHolder holder, int position) {

        holder.myPokedexText1.setText(data1[position]);
        holder.myPokedexText2.setText(data2[position]);
        holder.myPokedexText3.setText(data3[position]);
        holder.myPokedexImage.setImageResource(images[position]);

        holder.PokedexLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PokedexInfo.class);
                intent.putExtra("data1", data1[position]);
                intent.putExtra("data2", data2[position]);
                intent.putExtra("data3", data3[position]);
                intent.putExtra("PokedexInfoImage", images[position]);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class PokedexViewHolder extends RecyclerView.ViewHolder {

        TextView myPokedexText1, myPokedexText2, myPokedexText3;
        ImageView myPokedexImage;
        ConstraintLayout PokedexLayout;

        public PokedexViewHolder(@NonNull View itemView) {
            super(itemView);
            myPokedexText1 = itemView.findViewById(R.id.myPokedexText1);
            myPokedexText2 = itemView.findViewById(R.id.myPokedexText2);
            myPokedexText3 = itemView.findViewById(R.id.myPokedexText3);
            myPokedexImage = itemView.findViewById(R.id.myPokedexView);
            PokedexLayout = itemView.findViewById(R.id.PokedexLayout);
        }
    }
    }


