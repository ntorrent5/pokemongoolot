package com.example.prototippokemongo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PokedexInfo extends AppCompatActivity {
    private ImageView PokedexInfoImg;
    private TextView Nom,tipus,pc;

    private String data1, data2, data3;
    int PokedexInfoImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex_info);

        PokedexInfoImg =  findViewById(R.id.PokedexInfoImage);
        Nom =  findViewById(R.id.PokedexInfoText1);
        tipus =  findViewById(R.id.PokedexInfoText2);
        pc = findViewById(R.id.PokedexInfoText3);

        getData();
        setData();
    }

    private void getData(){
        if (getIntent().hasExtra("PokedexInfoImage") && getIntent().hasExtra("data1") &&
                getIntent().hasExtra("data2") && getIntent().hasExtra("data3")){

            data1 = getIntent().getStringExtra("data1");
            data2 = getIntent().getStringExtra("data2");
            data3 = getIntent().getStringExtra("data3");
            PokedexInfoImage = getIntent().getIntExtra("PokedexInfoImage", 1);
        } else {
            Toast.makeText(this, "No data", Toast.LENGTH_SHORT).show();
        }

    }

    private void setData(){
        Nom.setText(data1);
        tipus.setText(data2);
        pc.setText(data3);
        PokedexInfoImg.setImageResource(PokedexInfoImage);

    }
}