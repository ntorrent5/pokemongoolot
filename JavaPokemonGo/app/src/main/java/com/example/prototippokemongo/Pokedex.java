package com.example.prototippokemongo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Pokedex extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ImageView close;

    String s1[], s2[], s3[];
    int images[] = {R.drawable._85965d64f6ae202fedf2871,R.drawable._859661d4f6ae202fedf2877, R.drawable._80b57fcd9996e24bc43c325, R.drawable._85961604f6ae202fedf285a, R.drawable._80b57fcd9996e24bc43c31d,R.drawable._85966094f6ae202fedf2875,R.drawable._85965d64f6ae202fedf2871,R.drawable._859661d4f6ae202fedf2877,
            R.drawable._80b57fcd9996e24bc43c325, R.drawable._85961604f6ae202fedf285a, R.drawable._80b57fcd9996e24bc43c31d,R.drawable._85966094f6ae202fedf2875,R.drawable._85965d64f6ae202fedf2871,R.drawable._859661d4f6ae202fedf2877, R.drawable._80b57fcd9996e24bc43c325,
            R.drawable._85961604f6ae202fedf285a, R.drawable._80b57fcd9996e24bc43c31d,R.drawable._85966094f6ae202fedf2875};

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex);

        close = (ImageView) findViewById(R.id.closePokedex_button);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePokedex();
            }
        });

        recyclerView = findViewById(R.id.pokedexRecycler);

        s1 = getResources().getStringArray(R.array.pokemon_pokedex);
        s2 = getResources().getStringArray(R.array.tipus);
        s3 = getResources().getStringArray(R.array.Pc);

        PokedexAdapter pokedexAdapter = new PokedexAdapter(this, s1, s2, s3, images);
        recyclerView.setAdapter(pokedexAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
    }

    private void closePokedex() {
        Intent intent = new Intent(this, Menu.class);
        startActivity(intent);
    }
}
