package com.example.prototippokemongo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class Inventory extends AppCompatActivity {
    private ImageView close;
    private RecyclerView recyclerView;

    String s1[], s2[];
    int images[] = {R.drawable.objeto_pocion_pokemon_go,R.drawable.objeto_superpocion_pokemon_go, R.drawable.objeto_hiperpocion_pokemon_go, R.drawable.objeto_revivir_pokemon_go, R.drawable.objeto_incienso_pokemon_go,R.drawable.objeto_pokeball_pokemon_go, R.drawable.objeto_superball_pokemon_go, R.drawable.objeto_baya_frambu_pokemon_go};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);

        close = (ImageView) findViewById(R.id.closeInventory_button);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeInventory();
            }


        });

        recyclerView = findViewById(R.id.inventoryRecycler);

        s1 = getResources().getStringArray(R.array.pokemon_inventory);
        s2 = getResources().getStringArray(R.array.descripcio);

        InventoryAdapter inventoryAdapter = new InventoryAdapter(this, s1, s2, images);
        recyclerView.setAdapter(inventoryAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void closeInventory() {
        Intent intent = new Intent(this,Menu.class);
        startActivity(intent);
    }
}
