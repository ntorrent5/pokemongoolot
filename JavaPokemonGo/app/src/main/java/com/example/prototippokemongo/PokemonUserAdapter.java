package com.example.prototippokemongo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class PokemonUserAdapter extends RecyclerView.Adapter<PokemonUserAdapter.PokemonUserViewHolder> {

    String data1[], data2[], data3[];
    int images[];
    Context context;

    public PokemonUserAdapter(Context ct, String s1[], String s2[], String s3[], int img[]){

        context = ct;
        data1 = s1;
        data2 = s2;
        data3 = s3;
        images = img;

    }


    @Override
    public PokemonUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_pokemon_user, parent, false);
        return new PokemonUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonUserViewHolder holder, int position) {
        holder.myPokemonUserText1.setText(data1[position]);
        holder.myPokemonUserText2.setText(data2[position]);
        holder.myPokemonUserText3.setText(data3[position]);
        holder.myPokemonUserImage.setImageResource(images[position]);

        holder.PokemonUserLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PokemonUserInfo.class);
                intent.putExtra("data1", data1[position]);
                intent.putExtra("data2", data2[position]);
                intent.putExtra("data3", data3[position]);
                intent.putExtra("PokemonUserInfoImage", images[position]);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
       return images.length;
    }


    public class PokemonUserViewHolder extends RecyclerView.ViewHolder {
        TextView myPokemonUserText1, myPokemonUserText2, myPokemonUserText3;
        ImageView myPokemonUserImage;
        ConstraintLayout PokemonUserLayout;

        public PokemonUserViewHolder(@NonNull View itemView) {
            super(itemView);
            myPokemonUserText1 = itemView.findViewById(R.id.myPokemonUserText1);
            myPokemonUserText2 = itemView.findViewById(R.id.myPokemonUserText2);
            myPokemonUserText3 = itemView.findViewById(R.id.myPokemonUserText3);
            myPokemonUserImage = itemView.findViewById(R.id.myPokemonUserView);
            PokemonUserLayout = itemView.findViewById(R.id.PokemonUserLayout);
        }
    }
}
