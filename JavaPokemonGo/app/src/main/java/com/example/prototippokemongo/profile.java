package com.example.prototippokemongo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class profile extends AppCompatActivity {
    private ImageView close;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        close = (ImageView) findViewById(R.id.closeProfile_button);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeInventory();
            }
        });

    }
    public void closeInventory() {
        Intent intent = new Intent(this,Menu.class);
        startActivity(intent);
    }
}
