package com.example.prototippokemongo;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class PokemonUserInfo extends AppCompatActivity {

    private ImageView PokemonUserInfoImg;
    private TextView Nom,tipus,pc;

    private String data1, data2, data3;
    int PokemonUserInfoImage;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_user_info);

        PokemonUserInfoImg =  findViewById(R.id.PokemonUserInfoImage);
        Nom =  findViewById(R.id.PokemonUserInfoText1);
        tipus =  findViewById(R.id.PokemonUserInfoText2);
        pc = findViewById(R.id.PokemonUserInfoText3);

        getData();
        setData();

    }

    private void getData(){
        if (getIntent().hasExtra("PokemonUserInfoImage") && getIntent().hasExtra("data1") &&
                getIntent().hasExtra("data2") && getIntent().hasExtra("data3")){

                data1 = getIntent().getStringExtra("data1");
                data2 = getIntent().getStringExtra("data2");
                data3 = getIntent().getStringExtra("data3");
                PokemonUserInfoImage = getIntent().getIntExtra("PokemonUserInfoImage", 1);
        } else {
            Toast.makeText(this, "No data", Toast.LENGTH_SHORT).show();
        }

    }

    private void setData(){
        Nom.setText(data1);
        tipus.setText(data2);
        pc.setText(data3);
        PokemonUserInfoImg.setImageResource(PokemonUserInfoImage);

    }
}
